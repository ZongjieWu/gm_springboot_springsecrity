package com.wzj.springsecrity.admin.dto;

import lombok.Data;


@Data
public class UserBaseInfoDTO {
    /**
     * 电话号码（无用字段，先填username）
     */
    private String phone;

    /**
     *用户名
     */
    private String username;

    /**
     * token
     */
    private String token;
}
