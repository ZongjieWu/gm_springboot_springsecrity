package com.wzj.springsecrity.admin.dto;

import lombok.Data;


/**
 * 后台管理员基本信息实体响应数据
 */
@Data
public class SaasUserBaseInfoResponseVo {
    /**
     * 管理员id
     */
    private Long id;

    /**
     * 名称
     */
    private String name;


    /**
     * 名称
     */
    private String role;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
