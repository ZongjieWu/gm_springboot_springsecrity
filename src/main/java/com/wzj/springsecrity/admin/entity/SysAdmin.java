package com.wzj.springsecrity.admin.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author ZongjieWu
 * @since 2021-08-21
 */
@Data
//@EqualsAndHashCode(callSuper = false)
//@Accessors(chain = true)
@TableName("sys_admin")
public class SysAdmin implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * id
     */
    @TableId(value = "cod_id", type = IdType.AUTO)
    private Long id;

    /**
     * 名称
     */
    @TableField("cod_name")
    private String name;

    /**
     * 密码
     */
    @TableField("cod_password")
    private String password;

    /**
     * 角色
     */
    @TableField("cod_role")
    private String role;

    /**
     * 备注
     */
    @TableField("cod_memo")
    private String memo;

    /**
     * 创建时间
     */
    @TableField("cod_create_datetime")
    private LocalDateTime createDatetime;

    /**
     * 创建人
     */
    @TableField("cod_create_user")
    private String createUser;

    /**
     * 创建组织
     */
    @TableField("cod_create_org")
    private String createOrg;

    /**
     * 修改时间
     */
    @TableField("cod_modify_datetime")
    private LocalDateTime modifyDatetime;

    /**
     * 修改人
     */
    @TableField("cod_modify_user")
    private String modifyUser;

    /**
     * 修改组织
     */
    @TableField("cod_modify_org")
    private String modifyOrg;


    /**
     * 修改组织
     */
    @TableField("cod_delete_status")
    private Integer deleteStatus;

}
