package com.wzj.springsecrity.dto.c1;

import lombok.Data;

@Data
public class User {
    private String username;

    private String password;

    private boolean isAccountNonExpired;

    private boolean isAccountNonLocked;

    private boolean isCredentialsNonExpired;

    private boolean isEnabled;
}
