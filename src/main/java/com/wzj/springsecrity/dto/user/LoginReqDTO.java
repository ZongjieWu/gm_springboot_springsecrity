package com.wzj.springsecrity.dto.user;

import lombok.Data;

/**
 * 登入请求参数
 */
@Data
public class LoginReqDTO {
    /**
     * 电话号码
     */
    private String username;
    /**
     * 密码
     */
    private String password;
}
