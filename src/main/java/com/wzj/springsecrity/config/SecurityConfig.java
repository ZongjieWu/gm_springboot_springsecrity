package com.wzj.springsecrity.config;

import com.wzj.springsecrity.config.c1.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;


@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * 密码加密/无加密
     * @return
     */
//    @Bean
//    PasswordEncoder passwordEncoder() {
//        return NoOpPasswordEncoder.getInstance();
//    }

     @Bean
     PasswordEncoder passwordEncoder() {
     return new BCryptPasswordEncoder();
     }

// 基于内存配置用户信息
//     方式一、
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth
//        .inMemoryAuthentication()
//        .passwordEncoder(passwordEncoder())
//        .withUser("javaboy").password("123").roles("user");
//    }

//    //方式二
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.userDetailsService(userDetailsService());
//    }
//    @Bean
//    protected UserDetailsService userDetailsService() {
//        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
//        manager.createUser(User.withUsername("javaboy").password("123").roles("admin").build());
//        manager.createUser(User.withUsername("江南一点雨").password("123").roles("user").build());
//        return manager;
//    }

    /**
     * 方式三
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService());
    }

    @Bean
    protected UserDetailsService userDetailsService() {
        return new UserDetailService();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
//        web.ignoring().mvcMatchers("/js/**", "/css/**","/images/**","/login");
    }

    private CorsConfigurationSource CorsConfigurationSource() {
        CorsConfigurationSource source =   new UrlBasedCorsConfigurationSource();
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.addAllowedOrigin("*");	//同源配置，*表示任何请求都视为同源，若需指定ip和端口可以改为如“localhost：8080”，多个以“，”分隔；
        corsConfiguration.addAllowedHeader("*");//header，允许哪些header，本案中使用的是token，此处可将*替换为token；
        corsConfiguration.addAllowedMethod("*");	//允许的请求方法，PSOT、GET等
        ((UrlBasedCorsConfigurationSource) source).registerCorsConfiguration("/**",corsConfiguration); //配置允许跨域访问的url
        return source;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
            http.cors().configurationSource(CorsConfigurationSource());
            http
            .csrf().disable()
            .authorizeRequests()
                //对于静态资源的获取允许匿名访问
                .antMatchers(HttpMethod.GET, "/js/**", "/css/**","/images/**").permitAll()
                // 对登录注册要允许匿名访问;
//                .antMatchers("/login").anonymous()
                .anyRequest().authenticated()
            .and()
                .formLogin()
                .loginProcessingUrl("/admin/login").permitAll()
                .usernameParameter("username").passwordParameter("password")
                //登入成功操作
                .successHandler(new AuthenticationSuccessHandlerImpl())
                //登入失败操作
                .failureHandler(new AuthenticationFailureHandlerImpl())
            .and()
                .logout().logoutUrl("/admin/logout").permitAll()
                //登出后的返回结果
                .logoutSuccessHandler(new LogoutSuccessHandlerImpl())
                //这里配置的为当未登录访问受保护资源时，返回json
            .and()
                .exceptionHandling().authenticationEntryPoint(new AuthenticationEntryPointHandler());
    }
}
