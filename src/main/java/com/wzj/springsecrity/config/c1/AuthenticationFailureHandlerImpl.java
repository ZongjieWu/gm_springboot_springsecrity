package com.wzj.springsecrity.config.c1;

import com.wzj.springsecrity.dto.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.*;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@Slf4j
public class AuthenticationFailureHandlerImpl implements AuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        Result result = null;
        System.out.println("登入失败");
        String username = httpServletRequest.getParameter("username");
        String password = httpServletRequest.getParameter("password");

        BCryptPasswordEncoder passwordEncoder=new BCryptPasswordEncoder();
        log.info("加密密码{}",passwordEncoder.encode(password));
        log.info("账号{}，密码{}",username,password);

        if (e instanceof BadCredentialsException || e instanceof UsernameNotFoundException) {
            result = Result.returnCodeMsg(1001, "用户名或者密码错误");
        } else if (e instanceof LockedException) {
            result = Result.returnCodeMsg(1001, "错误：" + e.getMessage());
        } else if (e instanceof CredentialsExpiredException) {
            result = Result.returnCodeMsg(1001, "密码过期错误：" + e.getMessage());
        } else if (e instanceof AccountExpiredException) {
            result = Result.returnCodeMsg(1001, "账号过期错误：" + e.getMessage());
        } else if (e instanceof DisabledException) {
            result = Result.returnCodeMsg(1001, "禁用错误：" + e.getMessage());
        } else {
            result = Result.returnCodeMsg(1001, "其他错误：" + e.getMessage());
            log.info("用户[{}]于[{}]登录失败,失败原因：[{}]", username, new Date(), result.getMsg());
            log.info("用户密码[{}]于[{}]登录失败,失败原因：[{}]", password, new Date(), result.getMsg());

        }
        WriteResponse.write(httpServletResponse, result);
    }
}
