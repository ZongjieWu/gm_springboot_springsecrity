package com.wzj.springsecrity.config.c1;

import com.wzj.springsecrity.dto.Result;
import com.wzj.springsecrity.dto.c1.UserDetail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@Slf4j
public class LogoutSuccessHandlerImpl implements LogoutSuccessHandler {
    @Override
    public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        if (authentication != null) {
            UserDetail user=(UserDetail) authentication.getPrincipal();
            log.info("用户[{}]于[{}]注销成功!", user.getUsername(), new Date());
        }
        log.info("登出成功");
        WriteResponse.write(httpServletResponse, Result.returnSucessMsg("注销成功"));
    }
}
