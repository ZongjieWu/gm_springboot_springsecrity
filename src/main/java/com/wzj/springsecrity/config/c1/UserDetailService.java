package com.wzj.springsecrity.config.c1;

import com.wzj.springsecrity.admin.dao.SysAdminMapper;
import com.wzj.springsecrity.admin.dao.SysAuthoritiesMapper;
import com.wzj.springsecrity.admin.entity.SysAuthorities;
import com.wzj.springsecrity.dto.c1.UserDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserDetailService implements UserDetailsService {

    @Autowired
    private SysAdminMapper sysAdminMapper;

    @Autowired
    private SysAuthoritiesMapper sysAuthoritiesMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //查询用户身份
        UserDetail userDetail = sysAdminMapper.getUserDetailsByUserName(username);
        if (userDetail == null) {
            throw new UsernameNotFoundException("Not found username:" + username);
        }

        //设置用户权限
        //...
        List<SysAuthorities> authorities=sysAuthoritiesMapper.getByAdminId(userDetail.getId());
        userDetail.setAuthorities(authorities);
        return userDetail;
    }
}

