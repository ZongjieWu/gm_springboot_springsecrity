package com.wzj.springsecrity.config.c1;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wzj.springsecrity.dto.Result;
import org.springframework.http.MediaType;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class WriteResponse {
    private static final ObjectMapper mapper = new ObjectMapper();

    static void write(HttpServletResponse httpServletResponse, Result result) throws IOException {
        httpServletResponse.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
        PrintWriter out = httpServletResponse.getWriter();
        out.write(mapper.writeValueAsString(result));
        out.flush();
        out.close();
    }
}
