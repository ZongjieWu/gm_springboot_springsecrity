package com.wzj.springsecrity.config.c1;

import com.wzj.springsecrity.admin.dto.UserBaseInfoDTO;
import com.wzj.springsecrity.dto.Result;
import com.wzj.springsecrity.dto.c1.UserDetail;
import com.wzj.springsecrity.util.JWTUtils;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class AuthenticationSuccessHandlerImpl implements AuthenticationSuccessHandler {


    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        //登录成功后获取当前登录用户
        log.info("登入成功！！！");
        UserDetail userDetail = (UserDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username=userDetail.getUsername();
        String password=userDetail.getPassword();

        BCryptPasswordEncoder passwordEncoder=new BCryptPasswordEncoder();
        log.info("用户账号：[{}]于[{}]登录成功!", username, new Date());
        log.info("用户密码：[{}]于[{}]登录成功!密码：", password, new Date());
        log.info("用户加密密码：[{}]于[{}]登录成功!密码：", passwordEncoder.encode(password), new Date());

        UserBaseInfoDTO userBaseInfoDTO=generalJwt(userDetail);


        Result result=Result.returnSucessMsgData(userBaseInfoDTO);
        //登入成功返回token
        WriteResponse.write(httpServletResponse,result);

    }


    private UserBaseInfoDTO  generalJwt(UserDetail userDetail){
        //        自定义token
        //        String token = TokenUtil.createToken(new UserTokenDto(userSaasRes.getId(),2,0L));
        Map<String, Object> payload = new HashMap<String, Object>();
        payload.put("loginId", userDetail.getId());
        //86400000是过期时间  24小时
        String jwt=null;
        try {
            jwt = JWTUtils.createJWT("jwt", userDetail.getUsername(), 120000,payload);
        } catch (Exception e) {
            e.printStackTrace();
        }

        UserBaseInfoDTO userBaseInfoDTO=new UserBaseInfoDTO();
        userBaseInfoDTO.setPhone(userDetail.getUsername());
        userBaseInfoDTO.setUsername(userDetail.getUsername());
        userBaseInfoDTO.setToken(jwt);
        return userBaseInfoDTO;
    }


}
