package com.wzj.springsecrity.controller;

import com.wzj.springsecrity.dto.Result;
import com.wzj.springsecrity.dto.user.LoginReqDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 管理员
 */
@Slf4j
@RestController
@RequestMapping(value = "/admin")
public class TestController {

    /**
     * 登入请求
     * @param reqDTO
     * @return
     */
    @PostMapping("/login")
    public Result get( @Validated LoginReqDTO reqDTO){
        log.info("登入请求！！！");
        return Result.returnSucess();
    }

    /**
     * 登出请求
     * @return
     */
    @GetMapping("/logout")
    public Result logout(){
        log.info("登出请求！！！");
        return Result.returnSucess();
    }

    /**
     * 登入请求
     * @return
     */
    @PostMapping("/test")
    public Result test(){
        log.info("测试请求！！！");
        return Result.returnSucess();
    }
}
