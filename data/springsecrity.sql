/*
 Navicat Premium Data Transfer

 Source Server         : 本地数据库
 Source Server Type    : MySQL
 Source Server Version : 50721
 Source Host           : localhost:3306
 Source Schema         : springsecrity

 Target Server Type    : MySQL
 Target Server Version : 50721
 File Encoding         : 65001

 Date: 23/09/2021 22:51:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_admin
-- ----------------------------
DROP TABLE IF EXISTS `sys_admin`;
CREATE TABLE `sys_admin`  (
  `cod_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `cod_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `cod_password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `cod_role` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色',
  `cod_enabled` int(2) NULL DEFAULT NULL COMMENT '是否启用 1启用 2禁用',
  `cod_memo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `cod_create_datetime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `cod_create_user` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `cod_create_org` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建组织',
  `cod_modify_datetime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `cod_modify_user` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `cod_modify_org` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改组织',
  `cod_delete_status` int(1) NULL DEFAULT NULL COMMENT '1未删除 2已删除',
  PRIMARY KEY (`cod_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_admin
-- ----------------------------
INSERT INTO `sys_admin` VALUES (1, 'javaboy', '$2a$10$3ONXPnln1b7ZjrhlSKGP3O59xEz3ZXavVN4NTG/2xoieEodgboyjC', 'user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1);

-- ----------------------------
-- Table structure for sys_authorities
-- ----------------------------
DROP TABLE IF EXISTS `sys_authorities`;
CREATE TABLE `sys_authorities`  (
  `cod_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '本表id',
  `cod_admin_id` bigint(20) NULL DEFAULT NULL COMMENT 'admin表id',
  `cod_authority` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '权限',
  `cod_memo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `cod_create_datetime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `cod_create_user` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `cod_create_org` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建组织',
  `cod_modify_datetime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `cod_modify_user` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `cod_modify_org` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改组织',
  `cod_update_version` int(11) NULL DEFAULT NULL COMMENT '更新版本用作乐观锁',
  `cod_delete_status` int(1) NULL DEFAULT NULL COMMENT '1未删除 2已删除',
  PRIMARY KEY (`cod_id`) USING BTREE,
  UNIQUE INDEX `ix_auth_username`(`cod_authority`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_authorities
-- ----------------------------
INSERT INTO `sys_authorities` VALUES (1, 1, 'user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1);

SET FOREIGN_KEY_CHECKS = 1;
