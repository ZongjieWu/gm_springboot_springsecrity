import request from '@/utils/request'

/**
 * 登入
 * @param {} data
 * @returns
 */
export function login(data) {
  return request({
    url: '/admin/login',
    method: 'post',
    data,
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: [
      function(data) {
        var ret = ''
        for (var it in data) {
          ret = ret + encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
        }
        ret = ret.substring(0, ret.lastIndexOf('&'))
        return ret
      }
    ]
  })
}

/**
 *  根据token获取用户信息
 * @param {} token
 * @returns
 */
export function getInfo(token) {
  return request({
    url: '/saasUser/token',
    method: 'get',
    params: { token }
  })
}

/**
 * 退出
 */
export function logout() {
  return request({
    url: '/vue-admin-template/user/logout',
    method: 'post'
  })
}

/**
 * 添加管理员
 */
export function add() {
  return request({
    url: '/saasUser/add',
    method: 'post'
  })
}
