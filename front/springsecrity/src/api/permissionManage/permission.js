import request from '@/utils/request'

export function getList(params) {
  return request({
    url: '/saasUser/paging',
    method: 'post',
    params
  })
}
