import request from '@/utils/request'

/**
 * 获取管理员列表
 * @param {} params
 * @returns
 */
export function getList(params) {
  return request({
    url: '/saasUser/paging',
    method: 'post',
    params
  })
}

/**
 * 添加管理员
 */
export function add(data) {
  return request({
    url: '/saasUser/add',
    method: 'post',
    data
  })
}

/**
 * 修改管理员
 */
export function update(data) {
  return request({
    url: '/saasUser/update',
    method: 'post',
    data
  })
}
